\select@language {spanish}
\par \penalty \@M \unhbox \voidb@x \hbox {}\hfill {\bfseries P\'agina}\par \penalty \@M 
\contentsline {chapter}{\'{I}ndice de figuras}{iii}{section*.1}
\contentsline {chapter}{\chapternumberline {1}Introducci\'on}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Marco Te\'orico}{1}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Metabol\'omica}{2}{subsection.1.1.1}
\contentsline {subsubsection}{\numberline {1.1.1.1}Metabol\'omica direccionada}{3}{subsubsection.1.1.1.1}
\contentsline {subsubsection}{\numberline {1.1.1.2}Metabol\'omica no Direccionada}{4}{subsubsection.1.1.1.2}
\contentsline {subsection}{\numberline {1.1.2}T\'ecnicas anal\'iticas empleadas}{4}{subsection.1.1.2}
\contentsline {subsection}{\numberline {1.1.3}Cromatograf\'ia Gaseosa}{5}{subsection.1.1.3}
\contentsline {subsubsection}{\numberline {1.1.3.1}Cromatograf\'ia gaseosa bidimensional}{6}{subsubsection.1.1.3.1}
\contentsline {paragraph}{\numberline {1.1.3.1.1}Ventajas}{6}{paragraph.1.1.3.1.1}
\contentsline {subparagraph}{\numberline {1.1.3.1.1.1}Mejoramiento de se\~nal}{6}{subparagraph.1.1.3.1.1.1}
\contentsline {subparagraph}{\numberline {1.1.3.1.1.2}Cromatogramas estructurados}{7}{subparagraph.1.1.3.1.1.2}
\contentsline {subparagraph}{\numberline {1.1.3.1.1.3}Capacidad de pico}{9}{subparagraph.1.1.3.1.1.3}
\contentsline {paragraph}{\numberline {1.1.3.1.2}Modos de operaci\'on}{9}{paragraph.1.1.3.1.2}
\contentsline {subparagraph}{\numberline {1.1.3.1.2.1}Cromatograf\'ia gaseosa bidimensional parcial}{9}{subparagraph.1.1.3.1.2.1}
\contentsline {subparagraph}{\numberline {1.1.3.1.2.2}Cromatograf\'ia gaseosa bidimensional completa}{11}{subparagraph.1.1.3.1.2.2}
\contentsline {paragraph}{\numberline {1.1.3.1.3}Casos de estudio}{11}{paragraph.1.1.3.1.3}
\contentsline {subparagraph}{\numberline {1.1.3.1.3.1}Biofluido}{11}{subparagraph.1.1.3.1.3.1}
\contentsline {subsection}{\numberline {1.1.4}An\'alisis de datos $GC\times GC$ en metabol\'omica}{13}{subsection.1.1.4}
\contentsline {section}{\numberline {1.2}Estado del arte}{16}{section.1.2}
\contentsline {section}{\numberline {1.3}Planteamiento del problema}{18}{section.1.3}
\contentsline {section}{\numberline {1.4}Justificaci\'on}{18}{section.1.4}
\contentsline {section}{\numberline {1.5}Objetivos}{18}{section.1.5}
\contentsline {subsection}{\numberline {1.5.1}Objetivo general}{18}{subsection.1.5.1}
\contentsline {subsection}{\numberline {1.5.2}Objetivos espec\'ificos}{18}{subsection.1.5.2}
\contentsline {chapter}{\chapternumberline {2}Implementaci\'on del \textit {Software}}{21}{chapter.2}
\contentsline {section}{\numberline {2.1}Flujo de trabajo resumido}{21}{section.2.1}
\contentsline {section}{\numberline {2.2}Flujo de trabajo detallado}{22}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Instalaci\'on}{22}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Importaci\'on}{23}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Visualizaci\'on}{24}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}Preprocesamiento de se\~nal}{24}{subsection.2.2.4}
\contentsline {subsubsection}{\numberline {2.2.4.1}Correcci\'on de l\'inea base}{25}{subsubsection.2.2.4.1}
\contentsline {subsubsection}{\numberline {2.2.4.2}Suavizado}{25}{subsubsection.2.2.4.2}
\contentsline {subsubsection}{\numberline {2.2.4.3}Alinemiento de picos}{26}{subsubsection.2.2.4.3}
\contentsline {subsubsection}{\numberline {2.2.4.4}Uni\'on de chromatogramas}{27}{subsubsection.2.2.4.4}
\contentsline {subsection}{\numberline {2.2.5}An\'alisis multivariado}{27}{subsection.2.2.5}
\contentsline {subsubsection}{\numberline {2.2.5.1}\textit {Scores}}{28}{subsubsection.2.2.5.1}
\contentsline {subsubsection}{\numberline {2.2.5.2}\textit {Loadings}}{29}{subsubsection.2.2.5.2}
\contentsline {chapter}{Bibliograf\'{\i }a}{31}{section*.3}
