\let\textcircled=\pgftextcircled

\chapter{Implementaci\'on del \textit{Software}}

Este cap\'itulo describe la implementaci\'on del software denominado \textit{RGCxGC}.
Este \textit{software} es un paquete de R que permite realizar el an\'alisis completo
de datos de tipo GC$\times$GC-(TIC)MS. Este paquete puede ser encontrado
en la red completa de archivos de R (CRAN)
(\href{https://cran.r-project.org/web/packages/RGCxGC/index.html}
{https://cran.r-project.org/web/packages/RGCxGC/index.html}), y la versi\'on de
desarrollador en git-hub (\href{https://github.com/DanielQuiroz97/RGCxGC}
{https://github.com/DanielQuiroz97/RGCxGC}). Este paquete est\'a dise\~nado para
el manejo de datos en archivos de tipo NetCDF y plegar la se\~nal de corriente total de 
iones (TIC), en un cromatograma bidimensional. Despu\'es de esta conversi\'on, los datos est\'an
listos para realizar los an\'alisis subsecuentes.

El flujo general del an\'alisis de datos puede ser observado en la figura (\ref{fig:workflow}).
En el paquete RGCxGC, primero, el cromatograma se importa desde un archivo NetCDF y 
se pliega en el en un cromatograma bidimensional de Corriente Total de Iiones  (2D-TIC). A continuación,
puede realizar tres metodologías de preprocesamiento para eliminar el ruido: suavizado,
corrección de línea de base y alineación de picos. Básicamente, la mejora suave de la relación señal/ruido (S/N),
la corrección de línea de base maneja el sangrado de la columna y la alineación de picos corrige el cambio en el tiempo
de retención de los picos a lo largo de múltiples ejecuciones.
Finalmente, puede realizar un análisis de componentes principales de múltiples vías (MPCA) para buscar
patrones sistemáticos que distingan sus muestras.

\begin{figure}[ht!]
 \centering
 \includegraphics[width=0.9\linewidth]{./fig02/workflow.jpg}
 % workflow.png: 0x0 pixel, 300dpi, 0.00x0.00 cm, bb=
 \caption{Diagrama de flujo de an\'alisis de datos de GCxGC-MS que comprenden dos principales etapas,
          el preprocesamiento, y el an\'alisis multivariado. El diagrama empieza con la importaci\'on
          de datos crudos, despu\'es estos pueden ser preprocesados mediante tres t\'ecnicas,
          suavizado, alineamiento de picos, y por \'ultimo, realizar un an\'alisis multivariado.}
 \label{fig:workflow}
\end{figure}


\section{Flujo de trabajo resumido}
El flujo de trabajo del paquete \textit{RGCxGC} puede ser resumido en dos pasos principales, preprocesamiento y
an\'alisis multivariado (Figura. \ref{fig:functions}). En dicha figura, el nombre de las funciones se encuentran
encerradas en par\'entesis, mientras en el texto se encuentran tipo de letra \texttt{imprenta}.
El archivo \textit{NetCDF}  se importa con la función \texttt{read\_chrom}, en la cual el usuario necesita
configurar el tiempo de modulación en que se adquirieron los datos. A continuación, puede realizar el suavizado
y la corrección de la línea base utilizando la función \texttt{wsmooth} y \texttt{baseline\_corr}, respectivamente.
Luego, el alineamiento de picos de una muestra con respecto a una referencia se puede realizar mediante la función \texttt{twod\_cow} basada
en el algoritmo de distorsión optimizada de correlación bidimensional (2DCOW). Alternativamente,
la alineación de la muestra múltiple se puede realizar con la función \texttt{batch\_2DCOW}, donde el primer cromatograma
será considerado como referencia al alinear los cromatogramas restantes. Después del preprocesamiento,
se puede realizar MPCA en el conjunto de datos utilizando las funciones \texttt{m\_prcomp}, que proporcionan la matriz de 
\textit{loadings}, \textit{scores} y el resumen, que se pueden trazar utilizando las funciones \texttt{plot\_mpca} y \texttt{scores},
mientras que la función \texttt{print} se puede imprimir al resumen del MPCA.


\begin{figure}[ht!]
 \centering
 \includegraphics[width=0.9\linewidth]{./fig02/functions.png}
 % functions.png: 0x0 pixel, 300dpi, 0.00x0.00 cm, bb=
 \caption{Descripci\'on de las funcionalidades disponibles en el paquete \textit{RGCxGC}.
          El nombre de las funcionalidades se encuentra en cada recuadro, mientas en nombre de las
          rutinas en el paquete est\'an encerradas en par\'entesis.}
 \label{fig:functions}
\end{figure}


\section{Flujo de trabajo detallado}

  \subsection{Instalaci\'on}
   El paquete \textit{RGCxGC} puede ser instalado de varias maneras, la m\'as com\'un es instalarlo
   directamente desde CRAN.
   
   \begin{lstlisting}[language=R, frame=single]
install.packages("RGCxGC")
   \end{lstlisting}
   
   Otra opci\'on es instalar la versi\'on de desarrollador mediante el repositorio Github.
   
   \begin{lstlisting}[language=R, frame=single]
library(devtools)
install_github("DanQui97/RGCxGC")
   \end{lstlisting}
  
   Una vez se ha instalado correctamente el paquete, se puede acceder a las funciones y a los
   datos llamando a esta librer\'ia.
   
   \begin{lstlisting}[language=R, frame=single]
    library(RGCxGC)
   \end{lstlisting}
   
   
  \subsection{Importaci\'on}
  Los datos de ejemplo son tomados del estudio 
  \textit{Diagnostic metabolite biomarkers of chronic typhoid carriage} %\citep{Nasstrom2018}. Los
  chromatogramas fueron descargados de la base de datos MetaboLights con el n\'umero de acceso MTBLS579.
  
  Esta función lee el archivo NetCDF e importa los valores contenidos en las variables: \textit{total\_intensity} y
  \textit{acquisition\_time}. Primero, se eval\'ua si la frecuencia de adquisici\'on de datos es homog\'enea para
  despu\'es construir el cromatograma bidimensional. Esta rutina fue adaptada de \cite{Skov2008}.
  Los argumentos de esta funci\'on son los siguientes:
  
  \begin{itemize}
   \item \textit{name}: nombre del archivo tipo NetCDF de los cuales se importar\'an los datos.
   \item \textit{mod\_time}: tiempo de modulaci\'on de la corrida cromatogr\'afica.
   \item \textit{sam\_rate}: la taza de muestreo de la corrida cromatogr\'afica. Si no se provee este argumento,
                            la taza de muestreo es inferida mediante la divisi\'on deuno para la diferencia de dos
                            puntos adyacentes del tiempo de \textit{scan}.
   \item \textit{per\_eval}: Un decimal, con el porcentaje del total de valores para evaluar la homogeneidad de la taza
                            de adquisici\'on.
  \end{itemize}

  \textbf{Ejemplo}
  \begin{lstlisting}[language=R, frame=single]
GB08_fl <- system.file("extdata", "08GB.cdf", package = "RGCxGC")
GB08 <- read_chrom(GB08_fl, 5L)
   \end{lstlisting}
  
 
 
  \subsection{Visualizaci\'on}
  Para visualizar el cromatograma se puede utilizar la funci\'on \texttt{plot}. Se construye a partir de la funci\'on
  de R base de contorno, debido a que se inspira en en la visualizaci\'on de isolineas. Este tipo de visualizaci\'on aplicada para la
  representaci\'on de datos GCxGC no nativos \citep{Reichenbach2004}. La funci\'on por defecto solo grafica las se\~nales m\'as intensas.
  Para visualizar un cromatograma de mejor manera, debe proporcionar el color de la paleta y el n\'umero de niveles.

  Debido a la gran variedad de concentraciones relativas de metabolitos en una muestra, la se\~nal del TIC
  tambi\'en pueden tener una gran variabilidad en la escala de intensidad. Por lo tanto, se tiene que establecer el 
  n\'umero de niveles en escala de cientos. A medida que aumenta el n\'umero de niveles, se puede obtener un cromatograma 
  m\'as detallado. Por otro lado, puede utilizar los presentados en el paquete colorRamps (matlab.like $\&$ matlab.like2) 
  para la paleta de colores. Esta funci\'on tiene los siguientes argumentos:
  \begin{itemize}
   \item \textit{Object}: un cromatograma de tipo GC$\times$GC, que puede ser un cromatograma crudo o preprocesado.
   \item ... : otros par\'ametros empleados en la funci\'on \texttt{filled.contour}.
  \end{itemize}
   
   
   \textbf{Ejemplo}
     \begin{lstlisting}[language=R, frame=single]
library(colorRamps)
chrom_name <-  system.file("extdata", "08GB.cdf", package = "RGCxGC")
chrom_2D <- read_chrom(chrom_name, 5L)
plot(chrom_2D, nlevels = 150, color.palette = matlab.like)
   \end{lstlisting}
   
   
  \subsection{Preprocesamiento de se\~nal}
  En  cromatograf\'ia, las se\~nales  de los metabolitos a menudo est\'an ocultas por el ruido instrumental y qu\'imico. 
  Esta varianza de se\~nales indeseables perturba la interpretaci\'on qu\'imica de los resultados. 
  Para eliminar esta informaci\'on irrelevante, se pueden emplear algunos pasos de preprocesamiento. 
  Los algoritmos de preprocesamiento más comunes son: correcci\'on de l\'inea de base, suavizado y alineamiento de picos,
  que se revisar\'an en las siguientes secciones.
  
     \subsubsection{Correcci\'on de l\'inea base}
     La correcci\'on de l\'inea de base elimina una intensidad creciente constante en la se\~nal.
     Por ejemplo, contaminaci\'on de la inyecci\'on del sistema del instrumento, sangrado de la columna pueden
     ser causantes de la generaci\'on de l\'inea base. 
     La correcci\'on de l\'inea de base implementada en este paquete emplea el algoritmo de m\'inimos cuadrados 
     asim\'etricos \citep{Eilers2004}. Esta funci\'on tiene los siguientes argumentos:
     
     \begin{itemize}
      \item \textit{chromatogram}: un chromatograma de tipo GC$\times$GC.
     \end{itemize}
     
     \textbf{Ejemplo}
    \begin{lstlisting}[language=R, frame=single]
library(colorRamps)
chrom_name <- system.file("extdata", "08GB.cdf", package = "RGCxGC")
chrom_2D <- read_chrom(chrom_name, 5L)
chrom_bsline <- baseline_corr(chrom_2D)
plot(chrom_bsline, nlevels = 150, color.palette = matlab.like)
   \end{lstlisting}
     
     
     \subsubsection{Suavizado}
     El algoritmo de suavizado, con el nombre de  Whittaker fue introducido por \cite{Eilers2003}. 
     Como principales ventajas, se explica por la eficiencia computacional y el control sobre el suavizado, 
     la interpolaci\'on autom\'atica. El algoritmo Whittaker para GC$\times$GC se implementa en la primera dimensi\'on del cromatograma.
     En otras palabras, el suavizado se realiza en cada modulaci\'on.

     Este algoritmo trabaja con la funci\'on de mínimos cuadrados penalizados discretos.
     Entre los par\'ametros de esta rutina se tiene el orden de penalización (1 o 2) y $\lambda$,
     haciendo referencia a una penalizaci\'on lineal o cuadr\'atica, respectivamente. Por otro lado, $\lambda$ es un factor que multiplica
     el nivel de rugosidad de la se\~nal. Entonces, los valores de parámetros mayores tendr\'an un suavizado m\'as pronunciado.
     Los argumentos de esta funci\'on son:
     
     \begin{itemize}
      \item \textit{chromatogram}: Un cromatograma de tipo GC$\times$GC crudo o preprocesado.
      \item \textit{penalty}: Un entero con el orden de penalizaci\'on. Solo penalizaciones lineales
                              (penalty = 1) y cuadr\'aticas (penalty = 2) son permitidos. 
                              Por defecto, esta funci\'on tiene una penalizaci\'on de primer orden.
      \item \textit{lambda} par\'ametro de suavizado, mayores magnitudes conllevan a un mayor suavizado.
 
     \end{itemize}

     \textbf{Ejemplo}
         \begin{lstlisting}[language=R, frame=single]
chrom_name <- system.file("extdata", "08GB.cdf", package = "RGCxGC")
chrom_2D <- read_chrom(chrom_name, 5L)
chrom_smooth <- wsmooth(chrom_2D, penalty = 1, lambda = 1e1)
plot(chrom_smooth, nlevels = 150, color.palette = matlab.like,
     main = expression(paste(lambda, "= 10, penalty = 1")) )
         \end{lstlisting}
     
     \subsubsection{Alinemiento de picos}
     En el flujo del an\'alisis quimiom\'etrico, el usuario selecciona los pasos de preprocesamiento deseados.
     Sin embargo, el alineamiento de picos es pr\'acticamente obligatoria cuando se deben comparar m\'ultiples muestras.
     El alineamiento de picos corrige cambios inevitables de los tiempos de retenci\'on entre corridas cromatogr\'aficas.

     Hay dos v\'ias generales para el alineamiento de picos: usar la tabla de picos o el alineamiento a nivel de p\'ixeles.
     En este paquete, se implementa el alineamiento a nivel de p\'ixel. Se implementa el algoritmo de distorsión
     optimizada de por correlaci\'on   bidimensional (2D-COW) \citep{Zhang2008}. B\'asicamente,
     el algoritmo funciona dividiendo el cromatograma en $n$  segmentos, luego la distorsi\'on en el tiempo se aplica sobre
     cada dimensi\'on, utilizando la distorsi\'on optimizada  por correlaci\'on unidimensional (COW) \citep{Tomasi2004}.

      El \texttt{twod\_cow} necesita cuatro argumentos, la muestra y el cromatograma de referencia, 
      el n\'umero de segmentos para dividir el cromatograma y el nivel de deformaci\'on m\'aximo para ambas dimensiones
      
      \begin{itemize}
       \item \textit{sample\_chrom}: un cromatograma de tipo GC$\times$GC.
       \item \textit{ref\_chrom}: un cromatograma representativo el cual ser\'a tomado como referencia, y al cual
                                  el cromatograma de muestra ser\'a alineado
       \item \textit{segments}: un vector con dos enteros indicando el n\'umero de segmentos el cual ser\'an divididos
                                cada dimensi\'on.
       \item \textit{max\_warp}: un vector con dos enteros indicando la deformaci\'on m\'axima permitida.
      \end{itemize}
      
      \textbf{Ejemplo}
         \begin{lstlisting}[language=R, frame=single]
GB08_fl <- system.file("extdata", "08GB.cdf", package = "RGCxGC")
GB09_fl <- system.file("extdata", "09GB.cdf", package = "RGCxGC")
GB08 <- read_chrom(GB08_fl, 5L)
GB09 <- read_chrom(GB09_fl, 5L)
GB09_al <- twod_cow(GB09, GB08, c(20, 40), c(2, 8))
         \end{lstlisting}
      
     \subsubsection{Uni\'on de chromatogramas}
     Una vez que los cromatogramas est\'en preprocesados, tienen que estar almacenados en un solo objeto de R.
     Para cumplir con este requisito, se puede utilizar la func\'on \texttt{join\_chromatograms}.
     Adem\'as, si se tiene los metadatos, tambi\'en pueden unirse a este objeto.

     En esta funci\'on, puede unir tantos cromatogramas como el usuario desee.
     Sin embargo, se debe tener en cuenta es que, si proporciona m\'as de dos cromatogramas, se
     debe llamar a la funci\'on con un argumento nombrado. Por ejemplo, si tiene un cromatograma de nombre \textit{chrom\_control},
     la funci\'on debe ser llamada con un argumento nombrado \texttt{(chrom\_control = chrom\_control)}. Los
     argumentos de esta funci\'on son:
     
     \begin{itemize}
      \item \textit{x, y}: chromatogromas crudos, preprocesados \'unicos o en lote.
      \item \textit{groups}: un \texttt{data.frame} que contiene los metadatos. Esta debe tener un
                             una columna nombrada como \texttt{Name} para unir con los cromatogramas.
      \item ... : otros chromatogramas a ser unidos.
     \end{itemize}

     \textbf{Ejemplo}
         \begin{lstlisting}[language=R, frame=single]
GB08_fl <- system.file("extdata", "08GB.cdf", package = "RGCxGC")
GB09_fl <- system.file("extdata", "09GB.cdf", package = "RGCxGC")
GB08 <- read_chrom(GB08_fl, 5L)
GB09 <- read_chrom(GB09_fl, 5L)
join_gc <- join_chromatograms(GB08, GB09)
         \end{lstlisting}
         
   \subsection{An\'alisis multivariado}
   El an\'alisis multivariado es empleado para encontrar patrones sistem\'aticos entre las muestras.
   Debido a la alta complejidad de los cromatogramas que contienen miles de variables, los algoritmos multivariables
   son un enfoque interesante para el an\'alisis de datos en GC$\times$GC.

   En este caso, la extensi\'on del an\'alisis de componentes principales est\'a impl\'icita,
   el llamado an\'alisis de componentes principales en multiorden \citep{Wold1987}.
   Que puede manejar las tres dimensiones del conjunto de datos que es típico para 2D-TIC de GCxGC. 
   del experimento cromatogr\'afico est\'andar.

   Los datos de ejemplo constan de muestras portadoras de \textit{S. typhy} y el grupo de control.
   En este sentido vamos a realizar MPCA con 6 cromatogramas, los metadatos del experimento se resumen en 
   la tabla (\ref{tab:metadata}). Estos datos tratados pueden llamarse con el nombre de MTBLS579.
   
      \begin{table}[!htbp] \centering 
        \caption{Metadatos de los cromatogramas seleccionados provenientes de la base de datos 
                 MetaboLights, con el n\'umero de acceso MTBLS579.} 
        \label{tab:metadata} 
        \begin{tabular}{@{\extracolsep{5pt}} cc} 
        \\[-1.8ex]\hline 
        \hline \\[-1.8ex] 
        Nombre & Tipo \\ 
        \hline \\[-1.8ex] 
        08GB & Portador de  \textit{S. typhy} \\ 
        09GB & Portador de  \textit{S. typhy} \\ 
        14GB & Portador de  \textit{S. typhy} \\ 
        29GB & Control \\ 
        34GB & Control \\ 
        24GB & Control \\ 
        \hline \\[-1.8ex] 
        \end{tabular} 
        \end{table}
        
        El an\'alisis de componentes principales en multiorden puede realizar mediante la funci\'on
        \texttt{m\_prcomp}, con los siguientes argumentos:
        
        \begin{itemize}
         \item \textit{chrom}: m\'ultipes cromatogramas unidos con la funci\'on \texttt{join\_chromatograms}.
         \item \textit{center}: un valor booleano indicando si las variables deben ser centrados en la media.
                                por defecto este valor es configurado en erdadero, y se sugiere no ser cambiado.
         \item \textit{scale}: un valor booleano indicando si las variables deben ser escaladas hacia la varianza
                               unitaria. Por defecto, este valor es configurado a verdadero para dar una importancia
                               equilibrada a cada variable.
         \item \textit{npcs}:  un entero indicando cuantas componentes principales se desean mantener. Por defecto,
                               este valor es configurado a tres componentes principales.
         \item ... : otros par\'ametros que son pasados a la funci\'on nativa \texttt{prcomp}
        \end{itemize}

        \textbf{Ejemplo}

        \begin{lstlisting}[language=R, frame=single]
# Chromatogramas y metadata
data(MTBLS579)
MTBLS579_mpca <- m_prcomp(MTBLS579)
print(MTBLS579_mpca)
        \end{lstlisting}
        
        
      \subsubsection{\textit{Scores}}
      La matriz de \textit{scores} es la proyecci\'on de los cromatogramas en el espacio de componentes
      principales y se relaciona con las diferencias (cromatográficas) entre las muestras. Para trazar
      el acceso a la matriz de \textit{scores}. La matriz de \textit{scores} obtenida con los datos
      antes descritos se encuentra resumida en la tabla (\ref{tab:scores}).
      
      \begin{table}[!htbp] \centering 
      \caption{Matriz de \textit{scores} obtenida a partir de MPCA de los cromatogramas seleccionados.
               El an\'alisis fue llevado a cabo con datos centrados a la media y escalados a la unidad
               de varianza} 
       \label{tab:scores} 
       \begin{tabular}{@{\extracolsep{5pt}} cccc} 
       \\[-1.8ex]\hline 
       \hline \\[-1.8ex] 
       Names & Type & PC1 & PC2 \\ 
       \hline \\[-1.8ex] 
       08GB & Portador de  \textit{S. typhy} & -$190.77$ & -$42.11$ \\ 
       09GB & Portador de  \textit{S. typhy} & -$190.77$ & -$42.11$ \\ 
       14GB & Portador de  \textit{S. typhy} & -$209.19$ & $52.17$ \\ 
       24GB &            Control             &  $4.55$   & $50.43$ \\ 
       29GB &            Control             &  $293.09$ & -$9.18$ \\ 
       34GB &            Control             &  $293.09$ & -$9.18$ \\ 
       \hline \\[-1.8ex] 
       \end{tabular} 
       \end{table} 

       Esta funci\'on tiene el siguiente argumento \'unico:
       
       \begin{itemize}
        \item \textit{Object}: un objeto de tipo MPCA
       \end{itemize}

       \textbf{Ejemplo}
        \begin{lstlisting}[language=R, frame=single]
# Chromatogramas y metadata
data(MTBLS579)
# MPCA with mean-centered and scaled data
MTBLS579_mpca <- m_prcomp(MTBLS579, center = TRUE, scale = TRUE)
# Export scores matrix
scores(MTBLS579_mpca)
        \end{lstlisting}

        
      \subsubsection{\textit{Loadings}}
      Mientras que la matriz de \textit{scores} representa la relaci\'on entre las muestras, 
      la matriz de \textit{loadings} explica la relaci\'on entre las variables. A partir de los \textit{loadings}
      obtenemos un gr\'afico similar a los cromatogramas. Para visualizarlos se puede utilizar la función \texttt{plot\_loading}.
      El argumento de \texttt{type} se refiere a los valores de carga positivos (\texttt{tipo = "p"}) o
      negativos (\texttt{tipo = "n"}) o (\texttt{tipo = "b"}) para ambos  valores de \textit{loadings}.
      La opci\'on predeterminada de esta funci\'on es graficar la primera componente principal, 
      aunque se puede elegir cualquier PCs, en el argumento \texttt{pc}. 
      
      \begin{itemize}
       \item \textit{Object}: un objeto de tipo MPCA.
       \item \textit{type}: el tipo de valor de los \textit{loadings}, \texttt{p} para valores positivos,
                            \texttt{n} para negativos y \texttt{b} para ambos tipos de valores.
       \item \textit{pc}: el n\'umero de a componente principal para graficar.
       \item ... : otros argumentos pasados a la funci\'on nativa \texttt{filled.contour}.
      \end{itemize}

      
     


