=

Implementación del *Software*
=============================

Este capítulo describe la implementación del software denominado
*RGCxGC*. Este *software* es un paquete de R que permite realizar el
análisis completo de datos de tipo GC$\times$GC-(TIC)MS. Este paquete
puede ser encontrado en la red completa de archivos de R (CRAN)
(<https://cran.r-project.org/web/packages/RGCxGC/index.html>), y la
versión de desarrollador en git-hub
(<https://github.com/DanielQuiroz97/RGCxGC>). Este paquete está diseñado
para el manejo de datos en archivos de tipo NetCDF y plegar la señal de
corriente total de iones (TIC), en un cromatograma bidimensional.
Después de esta conversión, los datos están listos para realizar los
análisis subsecuentes.

El flujo general del análisis de datos puede ser observado en la figura
(\[fig:workflow\]). En el paquete RGCxGC, primero, el cromatograma se
importa desde un archivo NetCDF y se pliega en el en un cromatograma
bidimensional de Corriente Total de Iiones (2D-TIC). A continuación,
puede realizar tres metodologías de preprocesamiento para eliminar el
ruido: suavizado, corrección de línea de base y alineación de picos.
Básicamente, la mejora suave de la relación señal/ruido (S/N), la
corrección de línea de base maneja el sangrado de la columna y la
alineación de picos corrige el cambio en el tiempo de retención de los
picos a lo largo de múltiples ejecuciones. Finalmente, puede realizar un
análisis de componentes principales de múltiples vías (MPCA) para buscar
patrones sistemáticos que distingan sus muestras.

![Diagrama de flujo de análisis de datos de GCxGC-MS que comprenden dos
principales etapas, el preprocesamiento, y el análisis multivariado. El
diagrama empieza con la importación de datos crudos, después estos
pueden ser preprocesados mediante tres técnicas, suavizado, alineamiento
de picos, y por último, realizar un análisis multivariado.<span
data-label="fig:workflow"></span>](./fig02/workflow.jpg){width="0.9\linewidth"}

Flujo de trabajo resumido
-------------------------

El flujo de trabajo del paquete *RGCxGC* puede ser resumido en dos pasos
principales, preprocesamiento y análisis multivariado (Figura.
\[fig:functions\]). En dicha figura, el nombre de las funciones se
encuentran encerradas en paréntesis, mientras en el texto se encuentran
tipo de letra `imprenta`. El archivo *NetCDF* se importa con la función
`read_chrom`, en la cual el usuario necesita configurar el tiempo de
modulación en que se adquirieron los datos. A continuación, puede
realizar el suavizado y la corrección de la línea base utilizando la
función `wsmooth` y `baseline_corr`, respectivamente. Luego, el
alineamiento de picos de una muestra con respecto a una referencia se
puede realizar mediante la función `twod_cow` basada en el algoritmo de
distorsión optimizada de correlación bidimensional (2DCOW).
Alternativamente, la alineación de la muestra múltiple se puede realizar
con la función `batch_2DCOW`, donde el primer cromatograma será
considerado como referencia al alinear los cromatogramas restantes.
Después del preprocesamiento, se puede realizar MPCA en el conjunto de
datos utilizando las funciones `m_prcomp`, que proporcionan la matriz de
*loadings*, *scores* y el resumen, que se pueden trazar utilizando las
funciones `plot_mpca` y `scores`, mientras que la función `print` se
puede imprimir al resumen del MPCA.

![Descripción de las funcionalidades disponibles en el paquete *RGCxGC*.
El nombre de las funcionalidades se encuentra en cada recuadro, mientas
en nombre de las rutinas en el paquete están encerradas en
paréntesis.<span
data-label="fig:functions"></span>](./fig02/functions.png){width="0.9\linewidth"}

Flujo de trabajo detallado
--------------------------

### Instalación

El paquete *RGCxGC* puede ser instalado de varias maneras, la más común
es instalarlo directamente desde CRAN.

``` {.r language="R" frame="single"}
install.packages("RGCxGC")
   
```

Otra opción es instalar la versión de desarrollador mediante el
repositorio Github.

``` {.r language="R" frame="single"}
library(devtools)
install_github("DanQui97/RGCxGC")
   
```

Una vez se ha instalado correctamente el paquete, se puede acceder a las
funciones y a los datos llamando a esta librería.

``` {.r language="R" frame="single"}
    library(RGCxGC)
   
```

### Importación

Los datos de ejemplo son tomados del estudio *Diagnostic metabolite
biomarkers of chronic typhoid carriage* chromatogramas fueron
descargados de la base de datos MetaboLights con el número de acceso
MTBLS579.

Esta función lee el archivo NetCDF e importa los valores contenidos en
las variables: *total\_intensity* y *acquisition\_time*. Primero, se
evalúa si la frecuencia de adquisición de datos es homogénea para
después construir el cromatograma bidimensional. Esta rutina fue
adaptada de [@Skov2008]. Los argumentos de esta función son los
siguientes:

-   *name*: nombre del archivo tipo NetCDF de los cuales se importarán
    los datos.

-   *mod\_time*: tiempo de modulación de la corrida cromatográfica.

-   *sam\_rate*: la taza de muestreo de la corrida cromatográfica. Si no
    se provee este argumento, la taza de muestreo es inferida mediante
    la división deuno para la diferencia de dos puntos adyacentes del
    tiempo de *scan*.

-   *per\_eval*: Un decimal, con el porcentaje del total de valores para
    evaluar la homogeneidad de la taza de adquisición.

**Ejemplo**

``` {.r language="R" frame="single"}
GB08_fl <- system.file("extdata", "08GB.cdf", package = "RGCxGC")
GB08 <- read_chrom(GB08_fl, 5L)
   
```

### Visualización

Para visualizar el cromatograma se puede utilizar la función `plot`. Se
construye a partir de la función de R base de contorno, debido a que se
inspira en en la visualización de isolineas. Este tipo de visualización
aplicada para la representación de datos GCxGC no nativos
[@Reichenbach2004]. La función por defecto solo grafica las señales más
intensas. Para visualizar un cromatograma de mejor manera, debe
proporcionar el color de la paleta y el número de niveles.

Debido a la gran variedad de concentraciones relativas de metabolitos en
una muestra, la señal del TIC también pueden tener una gran variabilidad
en la escala de intensidad. Por lo tanto, se tiene que establecer el
número de niveles en escala de cientos. A medida que aumenta el número
de niveles, se puede obtener un cromatograma más detallado. Por otro
lado, puede utilizar los presentados en el paquete colorRamps
(matlab.like $\&$ matlab.like2) para la paleta de colores. Esta función
tiene los siguientes argumentos:

-   *Object*: un cromatograma de tipo GC$\times$GC, que puede ser un
    cromatograma crudo o preprocesado.

-   ... : otros parámetros empleados en la función `filled.contour`.

**Ejemplo**

``` {.r language="R" frame="single"}
library(colorRamps)
chrom_name <-  system.file("extdata", "08GB.cdf", package = "RGCxGC")
chrom_2D <- read_chrom(chrom_name, 5L)
plot(chrom_2D, nlevels = 150, color.palette = matlab.like)
   
```

### Preprocesamiento de señal

En cromatografía, las señales de los metabolitos a menudo están ocultas
por el ruido instrumental y químico. Esta varianza de señales
indeseables perturba la interpretación química de los resultados. Para
eliminar esta información irrelevante, se pueden emplear algunos pasos
de preprocesamiento. Los algoritmos de preprocesamiento más comunes son:
corrección de línea de base, suavizado y alineamiento de picos, que se
revisarán en las siguientes secciones.

#### Corrección de línea base

La corrección de línea de base elimina una intensidad creciente
constante en la señal. Por ejemplo, contaminación de la inyección del
sistema del instrumento, sangrado de la columna pueden ser causantes de
la generación de línea base. La corrección de línea de base implementada
en este paquete emplea el algoritmo de mínimos cuadrados asimétricos
[@Eilers2004]. Esta función tiene los siguientes argumentos:

-   *chromatogram*: un chromatograma de tipo GC$\times$GC.

**Ejemplo**

``` {.r language="R" frame="single"}
library(colorRamps)
chrom_name <- system.file("extdata", "08GB.cdf", package = "RGCxGC")
chrom_2D <- read_chrom(chrom_name, 5L)
chrom_bsline <- baseline_corr(chrom_2D)
plot(chrom_bsline, nlevels = 150, color.palette = matlab.like)
   
```

#### Suavizado

El algoritmo de suavizado, con el nombre de Whittaker fue introducido
por [@Eilers2003]. Como principales ventajas, se explica por la
eficiencia computacional y el control sobre el suavizado, la
interpolación automática. El algoritmo Whittaker para GC$\times$GC se
implementa en la primera dimensión del cromatograma. En otras palabras,
el suavizado se realiza en cada modulación.

Este algoritmo trabaja con la función de mínimos cuadrados penalizados
discretos. Entre los parámetros de esta rutina se tiene el orden de
penalización (1 o 2) y $\lambda$, haciendo referencia a una penalización
lineal o cuadrática, respectivamente. Por otro lado, $\lambda$ es un
factor que multiplica el nivel de rugosidad de la señal. Entonces, los
valores de parámetros mayores tendrán un suavizado más pronunciado. Los
argumentos de esta función son:

-   *chromatogram*: Un cromatograma de tipo GC$\times$GC crudo
    o preprocesado.

-   *penalty*: Un entero con el orden de penalización. Solo
    penalizaciones lineales (penalty = 1) y cuadráticas (penalty = 2)
    son permitidos. Por defecto, esta función tiene una penalización de
    primer orden.

-   *lambda* parámetro de suavizado, mayores magnitudes conllevan a un
    mayor suavizado.

**Ejemplo**

``` {.r language="R" frame="single"}
chrom_name <- system.file("extdata", "08GB.cdf", package = "RGCxGC")
chrom_2D <- read_chrom(chrom_name, 5L)
chrom_smooth <- wsmooth(chrom_2D, penalty = 1, lambda = 1e1)
plot(chrom_smooth, nlevels = 150, color.palette = matlab.like,
     main = expression(paste(lambda, "= 10, penalty = 1")) )
         
```

#### Alinemiento de picos

En el flujo del análisis quimiométrico, el usuario selecciona los pasos
de preprocesamiento deseados. Sin embargo, el alineamiento de picos es
prácticamente obligatoria cuando se deben comparar múltiples muestras.
El alineamiento de picos corrige cambios inevitables de los tiempos de
retención entre corridas cromatográficas.

Hay dos vías generales para el alineamiento de picos: usar la tabla de
picos o el alineamiento a nivel de píxeles. En este paquete, se
implementa el alineamiento a nivel de píxel. Se implementa el algoritmo
de distorsión optimizada de por correlación bidimensional (2D-COW)
[@Zhang2008]. Básicamente, el algoritmo funciona dividiendo el
cromatograma en $n$ segmentos, luego la distorsión en el tiempo se
aplica sobre cada dimensión, utilizando la distorsión optimizada por
correlación unidimensional (COW) [@Tomasi2004].

El `twod_cow` necesita cuatro argumentos, la muestra y el cromatograma
de referencia, el número de segmentos para dividir el cromatograma y el
nivel de deformación máximo para ambas dimensiones

-   *sample\_chrom*: un cromatograma de tipo GC$\times$GC.

-   *ref\_chrom*: un cromatograma representativo el cual será tomado
    como referencia, y al cual el cromatograma de muestra será alineado

-   *segments*: un vector con dos enteros indicando el número de
    segmentos el cual serán divididos cada dimensión.

-   *max\_warp*: un vector con dos enteros indicando la deformación
    máxima permitida.

**Ejemplo**

``` {.r language="R" frame="single"}
GB08_fl <- system.file("extdata", "08GB.cdf", package = "RGCxGC")
GB09_fl <- system.file("extdata", "09GB.cdf", package = "RGCxGC")
GB08 <- read_chrom(GB08_fl, 5L)
GB09 <- read_chrom(GB09_fl, 5L)
GB09_al <- twod_cow(GB09, GB08, c(20, 40), c(2, 8))
         
```

#### Unión de chromatogramas

Una vez que los cromatogramas estén preprocesados, tienen que estar
almacenados en un solo objeto de R. Para cumplir con este requisito, se
puede utilizar la funcón `join_chromatograms`. Además, si se tiene los
metadatos, también pueden unirse a este objeto.

En esta función, puede unir tantos cromatogramas como el usuario desee.
Sin embargo, se debe tener en cuenta es que, si proporciona más de dos
cromatogramas, se debe llamar a la función con un argumento nombrado.
Por ejemplo, si tiene un cromatograma de nombre *chrom\_control*, la
función debe ser llamada con un argumento nombrado
`(chrom_control = chrom_control)`. Los argumentos de esta función son:

-   *x, y*: chromatogromas crudos, preprocesados únicos o en lote.

-   *groups*: un `data.frame` que contiene los metadatos. Esta debe
    tener un una columna nombrada como `Name` para unir con
    los cromatogramas.

-   ... : otros chromatogramas a ser unidos.

**Ejemplo**

``` {.r language="R" frame="single"}
GB08_fl <- system.file("extdata", "08GB.cdf", package = "RGCxGC")
GB09_fl <- system.file("extdata", "09GB.cdf", package = "RGCxGC")
GB08 <- read_chrom(GB08_fl, 5L)
GB09 <- read_chrom(GB09_fl, 5L)
join_gc <- join_chromatograms(GB08, GB09)
         
```

### Análisis multivariado

El análisis multivariado es empleado para encontrar patrones
sistemáticos entre las muestras. Debido a la alta complejidad de los
cromatogramas que contienen miles de variables, los algoritmos
multivariables son un enfoque interesante para el análisis de datos en
GC$\times$GC.

En este caso, la extensión del análisis de componentes principales está
implícita, el llamado análisis de componentes principales en multiorden
[@Wold1987]. Que puede manejar las tres dimensiones del conjunto de
datos que es típico para 2D-TIC de GCxGC. del experimento cromatográfico
estándar.

Los datos de ejemplo constan de muestras portadoras de *S. typhy* y el
grupo de control. En este sentido vamos a realizar MPCA con 6
cromatogramas, los metadatos del experimento se resumen en la tabla
(\[tab:metadata\]). Estos datos tratados pueden llamarse con el nombre
de MTBLS579.

<span>@ cc</span>\
\
Nombre & Tipo\
\
08GB & Portador de *S. typhy*\
09GB & Portador de *S. typhy*\
14GB & Portador de *S. typhy*\
29GB & Control\
34GB & Control\
24GB & Control\
\

El análisis de componentes principales en multiorden puede realizar
mediante la función `m_prcomp`, con los siguientes argumentos:

-   *chrom*: múltipes cromatogramas unidos con la función
    `join_chromatograms`.

-   *center*: un valor booleano indicando si las variables deben ser
    centrados en la media. por defecto este valor es configurado en
    erdadero, y se sugiere no ser cambiado.

-   *scale*: un valor booleano indicando si las variables deben ser
    escaladas hacia la varianza unitaria. Por defecto, este valor es
    configurado a verdadero para dar una importancia equilibrada a
    cada variable.

-   *npcs*: un entero indicando cuantas componentes principales se
    desean mantener. Por defecto, este valor es configurado a tres
    componentes principales.

-   ... : otros parámetros que son pasados a la función nativa `prcomp`

**Ejemplo**

``` {.r language="R" frame="single"}
# Chromatogramas y metadata
data(MTBLS579)
MTBLS579_mpca <- m_prcomp(MTBLS579)
print(MTBLS579_mpca)
        
```

#### *Scores*

La matriz de *scores* es la proyección de los cromatogramas en el
espacio de componentes principales y se relaciona con las diferencias
(cromatográficas) entre las muestras. Para trazar el acceso a la matriz
de *scores*. La matriz de *scores* obtenida con los datos antes
descritos se encuentra resumida en la tabla (\[tab:scores\]).

<span>@ cccc</span>\
\
Names & Type & PC1 & PC2\
\
08GB & Portador de *S. typhy* & -$190.77$ & -$42.11$\
09GB & Portador de *S. typhy* & -$190.77$ & -$42.11$\
14GB & Portador de *S. typhy* & -$209.19$ & $52.17$\
24GB & Control & $4.55$ & $50.43$\
29GB & Control & $293.09$ & -$9.18$\
34GB & Control & $293.09$ & -$9.18$\
\

Esta función tiene el siguiente argumento único:

-   *Object*: un objeto de tipo MPCA

**Ejemplo**

``` {.r language="R" frame="single"}
# Chromatogramas y metadata
data(MTBLS579)
# MPCA with mean-centered and scaled data
MTBLS579_mpca <- m_prcomp(MTBLS579, center = TRUE, scale = TRUE)
# Export scores matrix
scores(MTBLS579_mpca)
        
```

#### *Loadings*

Mientras que la matriz de *scores* representa la relación entre las
muestras, la matriz de *loadings* explica la relación entre las
variables. A partir de los *loadings* obtenemos un gráfico similar a los
cromatogramas. Para visualizarlos se puede utilizar la función
`plot_loading`. El argumento de `type` se refiere a los valores de carga
positivos (`tipo = p`) o negativos (`tipo = n`) o (`tipo = b`) para
ambos valores de *loadings*. La opción predeterminada de esta función es
graficar la primera componente principal, aunque se puede elegir
cualquier PCs, en el argumento `pc`.

-   *Object*: un objeto de tipo MPCA.

-   *type*: el tipo de valor de los *loadings*, `p` para valores
    positivos, `n` para negativos y `b` para ambos tipos de valores.

-   *pc*: el número de a componente principal para graficar.

-   ... : otros argumentos pasados a la función nativa `filled.contour`.


